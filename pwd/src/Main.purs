module Main where

import Prelude
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Node.Process (PROCESS, cwd)

main :: forall e. Eff (console :: CONSOLE, process :: PROCESS | e) Unit
main = do
  pwd <- cwd
  log pwd
