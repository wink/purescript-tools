module Main where

import Prelude
import Data.Array (head, last, tail)
import Data.Maybe (Maybe(..))
import Data.String (Pattern(..), split)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE) --, log, logShow)
import Node.Process (PROCESS, exit, argv)

hd :: Array String -> String
hd x = case head x of
  Nothing -> ""
  Just h -> h

second :: Array String -> String
second x = hd a
  where
    a = case tail x of
     Nothing -> []
     Just h -> h


getExitCodeByFilename :: String -> Int
getExitCodeByFilename s =
  case last parts of
    Just "false" -> 23
    _ -> 0
  where
    parts = split (Pattern "/") s

main :: Eff (process :: PROCESS, console :: CONSOLE) Unit
main = do
  args <- argv
  -- logShow args
  exit $ getExitCodeByFilename $ second args
