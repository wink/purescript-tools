module Test.Main where

import Control.Monad.Eff.Console (log)
import Prelude (bind)
import Test.Spec (it, describe)
import Test.Spec.Assertions (shouldEqual)
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (run)

import Main (joinPath, joinPaths)

main = run [consoleReporter] do
  joinPathTest
  joinPathsTest


joinPathTest = describe "joinPath" do
  it "should join empty path and base" do
    shouldEqual (joinPath "" "foo") "foo"

  it "should join non-empty path and base" do
    shouldEqual (joinPath "/tmp" "foo") "/tmp/foo"

  it "should join path and non-empty base" do
    shouldEqual (joinPath "/tmp" "") "/tmp"


joinPathsTest = describe "joinPaths" do
  it "should return empty" do
    shouldEqual (joinPaths [] "bar") []

  it "should do nothing with empty file" do
    shouldEqual (joinPaths ["/tmp", "/foo"] "") ["/tmp", "/foo"]

  it "should append file to all items" do
    shouldEqual (joinPaths ["/tmp", "/foo"] "bar") ["/tmp/bar", "/foo/bar"]
