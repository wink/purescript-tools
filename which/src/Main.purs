module Main where

import Prelude
import Control.Error.Util
import Control.Apply ((*>))
import Data.Array (head, last, tail, index, filter)
import Data.Either
import Data.Int as I
import Data.Maybe (Maybe(..), maybe, fromMaybe)
import Data.String (Pattern(..), split)
import Data.Traversable (traverse, for, traverseDefault)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Class
import Control.Monad.Eff.Console (CONSOLE, log, logShow, error)
import Control.Monad.Eff.Exception (EXCEPTION, catchException, try)
import Node.FS (FS)
import Node.FS.Stats (Stats, isFile)
import Node.FS.Sync as S
import Node.Path as Path
import Node.Process (PROCESS, exit, argv, lookupEnv)


joinPath :: String -> String -> String
joinPath dir base = Path.concat [dir, base]

joinPaths :: Array String -> String -> Array String
joinPaths dirs file = map (flip joinPath file) dirs

getPath1 :: forall eff. Eff ( process :: PROCESS | eff ) (Maybe String)
getPath1 = lookupEnv "PATH"

type Foo = { name :: String, isFile :: Boolean }

valid :: Foo -> Boolean
valid x = (x.isFile == true)

getName :: Foo -> String
getName x = x.name

checkFile3 t = do
  x <- try (S.stat t)
  case x of
    Right r -> pure $ {name: t, isFile: (isFile r)}
    Left err -> pure $ {name: t, isFile: false}

main :: Eff (process :: PROCESS, fs :: FS, err :: EXCEPTION, console :: CONSOLE) Unit
main = do
  args <- argv
  let file = fromMaybe "" (index args 2)
  gp1 <- getPath1
  let gp2 = maybe [] (split (Pattern ":")) gp1
  let gp3 = joinPaths gp2 file
  b <- traverseDefault checkFile3 gp3
  log $ getName $ fromMaybe {name:"", isFile: true} $ head $ (filter valid b)
