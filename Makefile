
all: true_false dist/which dist/pwd

true_false: dist/true dist/false

dist/true:
	-mkdir dist
	cd true && TMPDIR=../dist pulp build -O --to ../dist/true

dist/false:
	cp dist/true dist/false

dist/which:
	-mkdir dist
	cd which && TMPDIR=../dist pulp build -O --to ../dist/which

dist/pwd:
	-mkdir dist
	cd pwd && TMPDIR=../dist pulp build -O --to ../dist/pwd

clean:
	rm dist/*

.PHONY: clean
